import React, { useState, useEffect } from 'react'
import './BeliPage.css'

 function BeliPage() {
    // state hook pada functional component
    const [selectedProducts, selectProduct] = useState([])

    // effect hook pada functional component
    useEffect(() => {
        document.getElementById("selected-products").innerHTML = selectedProducts.join(', ')
    })

    const cpus = [
        {
            id: '0001',
            name: 'i5-10600KF',
            price: 2000000,
        },
        {
            id: '0002',
            name: 'Ryzen 5 3600',
            price: 1800000,
        }
    ]
    let rows = []

    for (let i = 0; i < cpus.length; i++) {
        rows.push(
            <tr>
                <td>{cpus[i].name}</td>
                <td>{cpus[i].price}</td>
                <td><button onClick={(e) => { selectProduct([...selectedProducts, e.target.id]) }} className="btn btn-sm btn-primary" id={cpus[i].id}>Pilih</button></td>
            </tr>
        )
    }

    return (
        <div className="container">
            <h2>Daftar Produk yang Tersedia</h2>
            <p><small>ID produk yang Anda pilih: <span id="selected-products"></span></small></p>
            <table className="table table-product">
                <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                {rows}
            </table>
        </div>
    )
}

export default BeliPage;